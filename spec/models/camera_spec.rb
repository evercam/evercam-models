require 'data_helper'

describe Camera do

  let(:camera) { create(:camera) }

  describe '::by_exid!' do

    it 'returns the camera when it exists' do
      expect(Camera.by_exid!(camera.exid)).to eq(camera)
    end

    it 'raises a NotFoundError when it does not exist' do
      expect{ Camera.by_exid!('xxxx') }.
        to raise_error(Evercam::NotFoundError)
    end

  end

  describe '::by_distance' do

    let(:evercam) { { lng: -6.2629, lat: 53.3543 } }

    it 'returns cameras within a given radius' do
      dublin = create(:camera, location: { lng: -6.2608, lat: 53.3493 })
      expect(Camera.by_distance(evercam, 1000).all).to eq([dublin])
    end

    it 'does not return cameras outside a given radius' do
      london = create(:camera, location: { lng: -0.1281, lat: 51.5080 })
      expect(Camera.by_distance(evercam, 1000).all).to eq([])
    end

    it 'can be concatenated to an other query' do
      dublin = create(:camera, location: { lng: -6.2608, lat: 53.3493 })
      query = Camera.where(is_public: false).and(is_online: false)
      expect(query.by_distance(evercam, 1000).all).to eq([])
    end

  end

  describe '#allow?' do

    it 'is true for all rights when the auth is the owner' do
      owner = camera.owner
      token = owner.token
      expect(camera.allow?(AccessRight::VIEW, camera.owner.token)).to eq(true)
    end

    describe 'snapshot right' do

      it 'is true when the camera is public' do
        camera.update(is_public: true)
        expect(camera.allow?(AccessRight::SNAPSHOT, nil)).to eq(true)
      end

    end

    describe 'view right' do

      context 'when the camera is not public' do

        let(:user) { create(:user) }
        let(:access_token) { create(:access_token, user_id: user.id) }

        before(:each) do
          camera.update(is_public: false)
        end

        it 'is false when auth is nil' do
          expect(camera.allow?(AccessRight::VIEW, nil)).to eq(false)
        end

        it 'is true when auth includes specific camera scope' do
          expect(camera.allow?(AccessRight::VIEW, access_token)).to eq(false)
          AccessRightSet.for(camera, user).grant(AccessRight::VIEW)
          expect(camera.allow?(AccessRight::VIEW, access_token)).to eq(true)
        end

        it 'is false when no token is provided' do
          expect(camera.allow?(AccessRight::VIEW, nil)).to eq(false)
        end

      end

    end

  end

  describe '#timezone' do

    it 'defaults to UTC when no zone is specified' do
      expect(build(:camera, timezone: nil).timezone).
        to eq(Timezone::Zone.new zone: 'Etc/UTC')
    end

    it 'returns the correct zone instance when on is set' do
      expect(build(:camera, timezone: 'America/Chicago').timezone).
        to eq(Timezone::Zone.new zone: 'America/Chicago')
    end

  end

  describe '#url' do

    it 'returns full url including owner username and camera exid' do
      expect(camera.url).to eq("/users/#{camera.owner.username}/cameras/#{camera.exid}")
    end

  end

  describe '#vendor' do

    it 'defaults to null' do
      expect(camera.vendor).to be_nil
    end

  end

  describe '#is_mac_address?' do

    it 'correctly validates strings as mac address' do
      expect(Camera.is_mac_address?('')).to be_falsey
      expect(Camera.is_mac_address?('aaa')).to be_falsey
      expect(Camera.is_mac_address?('01-23-45-67-89-ab')).to be_truthy
      expect(Camera.is_mac_address?('01:23:45:67:89:ab')).to be_truthy
    end

  end

  describe '#cam_username' do

    it 'defaults to empty string when no auth is specified' do
      expect(build(:camera, config: {}).cam_username).to eq('')
    end

    it 'returns the correct username when it is set' do
      expect(build(:camera).cam_username).to eq('abcd')
    end

  end

  describe '#cam_password' do

    it 'defaults to empty string when no auth is specified' do
      expect(build(:camera, config: {}).cam_password).to eq('')
    end

    it 'returns the correct username when it is set' do
      expect(build(:camera).cam_password).to eq('wxyz')
    end

  end

  describe '#config' do

    let(:model0) { create(:vendor_model, config: { 'a' => 'xxxx' }) }

    it 'returns camera config if model is nil' do
      d0 = create(:camera, vendor_model: nil, config: { 'a' => 'zzzz' })
      expect(d0.config).to eq({ 'a' => 'zzzz' })
    end

    it 'merges its config with that of its model' do
      d0 = create(:camera, vendor_model: model0, config: { 'b' => 'yyyy' })
      expect(d0.config).to eq({ 'a' => 'xxxx', 'b' => 'yyyy'})
    end

    it 'gives precedence to values from the camera config' do
      d0 = create(:camera, vendor_model: model0, config: { 'a' => 'yyyy' })
      expect(d0.config).to eq({ 'a' => 'yyyy' })
    end

    it 'deep merges where both camera have the same keys' do
      model0.update(config: { 'a' => { 'b' => 'xxxx' } })
      d0 = create(:camera, vendor_model: model0, config: { 'a' => { 'c' => 'yyyy' } })
      expect(d0.config).to eq({ 'a' => { 'b' => 'xxxx', 'c' => 'yyyy' } })
    end

  end

  describe '#location' do

    let(:point) { '0101000020E610000000000000000024400000000000003440' }

    it 'returns nil when no location set' do
      expect(camera.location).to be_nil
    end

    it 'returns a GeoRuby Point when location is set' do
      camera.values[:location] = point
      expect(camera.location.x).to eq(10.0)
    end

    it 'sets the location to nil when nil passed' do
      camera.location = nil
      expect(camera.values[:location]).to be_nil
    end

    it 'sets the location value from a GeoRuby Point' do
      camera.location = GeoRuby::SimpleFeatures::Point.from_hex_ewkb(point)
      expect(camera.values[:location]).to eq(point)
    end

    it 'sets the location when passed a lng lat hash' do
      camera.location = { lng: 10, lat: 20 }
      expect(camera.values[:location]).to eq(point)
    end

  end

  describe '#model' do

    context 'when a model is specifically set' do
      it 'returns that specific model' do
        model = create(:vendor_model)
        camera.update(vendor_model: model)
        expect(camera.vendor_model).to eq(model)
      end
    end

    context 'when a model is not specifically set' do

      context 'when the mac address matches a supported vendor' do
        it 'returns the default model' do
          vendor = create(:vendor, known_macs: ['8C:E7:48'])
          model = create(:vendor_model, vendor: vendor, name: VendorModel::DEFAULT)

          camera.update(vendor_model: nil, mac_address: '8c:e7:48:bd:bd:f5')
          expect(camera.vendor_model).to eq(model)
        end
      end

      context 'when the mac address does not match a supported vendor' do
        it 'return nil' do
          camera.update(vendor_model: nil, mac_address: '8c:e7:48:bd:bd:f5')
          expect(camera.vendor_model).to be_nil
        end
      end

      context 'when the mac address is nil' do
        it 'return nil' do
          camera.update(vendor_model: nil, mac_address: nil)
          expect(camera.vendor_model).to be_nil
        end
      end

    end

  end

  describe 'urls' do
    describe 'external' do

      context 'when external host is empty' do
        it 'returns null' do
          camera.values[:config].merge!({'external_host' => nil})
          expect(camera.external_url).to be_nil
        end
      end

      context 'when external host is not empty, but port is empty' do
        it 'returns url without port' do
          camera.values[:config].merge!({'external_host' => '1.1.1.1'})
          camera.values[:config].merge!({'external_http_port' => nil})
          expect(camera.external_url).to eq('http://1.1.1.1')
        end
      end

      context 'when external host is not empty and port is not empty' do
        it 'returns url with port' do
          camera.values[:config].merge!({'external_host' => '1.1.1.1'})
          camera.values[:config].merge!({'external_http_port' => 123})
          expect(camera.external_url).to eq('http://1.1.1.1:123')
        end
      end


      context 'when external host is not empty, but rtsp port is empty' do
        it 'returns url without port' do
          camera.values[:config].merge!({'external_host' => '1.1.1.1'})
          camera.values[:config].merge!({'external_rtsp_port' => nil})
          expect(camera.external_url(type='rtsp')).to eq('rtsp://1.1.1.1')
        end
      end

      context 'when external host is not empty and port is not empty' do
        it 'returns url with port' do
          camera.values[:config].merge!({'external_host' => '1.1.1.1'})
          camera.values[:config].merge!({'external_rtsp_port' => 124})
          expect(camera.external_url(type='rtsp')).to eq('rtsp://1.1.1.1:124')
        end
      end

    end

    describe 'internal' do

      context 'when internal host is empty' do
        it 'returns null' do
          camera.values[:config].merge!({'internal_host' => nil})
          expect(camera.internal_url).to be_nil
        end
      end

      context 'when internal host is not empty, but port is empty' do
        it 'returns url without port' do
          camera.values[:config].merge!({'internal_host' => '1.1.1.1'})
          camera.values[:config].merge!({'internal_http_port' => nil})
          expect(camera.internal_url).to eq('http://1.1.1.1')
        end
      end

      context 'when internal host is not empty and port is not empty' do
        it 'returns url with port' do
          camera.values[:config].merge!({'internal_host' => '1.1.1.1'})
          camera.values[:config].merge!({'internal_http_port' => 123})
          expect(camera.internal_url).to eq('http://1.1.1.1:123')
        end
      end


      context 'when internal host is not empty, but rtsp port is empty' do
        it 'returns url without port' do
          camera.values[:config].merge!({'internal_host' => '1.1.1.1'})
          camera.values[:config].merge!({'internal_http_port' => nil})
          expect(camera.internal_url(type='rtsp')).to eq('rtsp://1.1.1.1')
        end
      end

      context 'when internal host is not empty and port is not empty' do
        it 'returns url with port' do
          camera.values[:config].merge!({'internal_host' => '1.1.1.1'})
          camera.values[:config].merge!({'internal_rtsp_port' => 124})
          expect(camera.internal_url(type='rtsp')).to eq('rtsp://1.1.1.1:124')
        end
      end

    end

    describe 'dyndns' do

      context 'when external host is empty' do
        it 'returns null' do
          camera.values[:config].merge!({'external_host' => nil})
          expect(camera.dyndns_url).to be_nil
        end
      end

      context 'when external host is not empty, but port is empty' do
        it 'returns url without port' do
          camera.values[:config].merge!({'external_host' => '1.1.1.1'})
          camera.values[:config].merge!({'external_http_port' => nil})
          expect(camera.dyndns_url).to eq("http://#{camera.exid}.evr.cm")
        end
      end

      context 'when external host is not empty and port is not empty' do
        it 'returns url with port' do
          camera.values[:config].merge!({'external_host' => '1.1.1.1'})
          camera.values[:config].merge!({'external_http_port' => 123})
          expect(camera.dyndns_url).to eq("http://#{camera.exid}.evr.cm:123")
        end
      end


      context 'when external host is not empty, but rtsp port is empty' do
        it 'returns url without port' do
          camera.values[:config].merge!({'external_host' => '1.1.1.1'})
          camera.values[:config].merge!({'external_rtsp_port' => nil})
          expect(camera.dyndns_url(type='rtsp')).to eq("rtsp://#{camera.exid}.evr.cm")
        end
      end

      context 'when external host is not empty and port is not empty' do
        it 'returns url with port' do
          camera.values[:config].merge!({'external_host' => '1.1.1.1'})
          camera.values[:config].merge!({'external_rtsp_port' => 124})
          expect(camera.dyndns_url(type='rtsp')).to eq("rtsp://#{camera.exid}.evr.cm:124")
        end
      end

    end

    describe 'resource urls' do
      context 'when resource is requested with or without slash in front' do
        it 'returns path with slash or nil' do
          camera.values[:config].merge!({'snapshots' => {'jpg' => '/a.jpg', 'h264' => 'h'}})
          expect(camera.res_url('jpg')).to eq('/a.jpg')
          expect(camera.res_url('h264')).to eq('/h')
          expect(camera.res_url('mpeg')).to eq('')
        end
      end
    end

  end

  describe 'discoverable?' do
    it 'returns false by default' do
      camera.config.delete("discoverable")
      expect(camera.discoverable?).to eq(false)
    end

    it 'returns true only when set to true' do
      camera.config["discoverable"] = true
      expect(camera.discoverable?).to eq(false)

      camera.config["discoverable"] = false
      expect(camera.discoverable?).to eq(false)

      camera.config["discoverable"] = 100
      expect(camera.discoverable?).to eq(false)

      camera.config["discoverable"] = "blah"
      expect(camera.discoverable?).to eq(false)
    end
  end

  describe 'validations' do
    let!(:owner) { create(:user) }

    it 'creates a valid object when all field values are valid' do
      camera = Camera.new(exid: 'new-test-camera-01',
                          owner: owner,
                          is_public: false,
                          config: {},
                          name: 'Camera Name',
                          discoverable: false)
      expect(camera.valid?).to eq(true)
    end

    it 'creates an invalid object if an invalid exid value is specified' do
      camera = Camera.new(exid: 'new-test-camera>01',
                          owner: owner,
                          is_public: false,
                          config: {},
                          name: 'Camera Name',
                          discoverable: false)
      expect(camera.valid?).to eq(false)
      expect(camera.errors.size).to eq(1)
      expect(camera.errors.include?(:exid)).to eq(true)
      expect(camera.errors[:exid]).to eq(["is invalid"])
    end

    it 'creates an valid object if an name value with special characters is specified' do
      camera = Camera.new(exid: 'new-test-camera-01',
                          owner: owner,
                          is_public: false,
                          config: {},
                          name: 'Camera Name\\',
                          discoverable: false)
      expect(camera.valid?).to eq(true)
    end
  end

end

