require 'data_helper'

describe CameraShareRequest do
   let(:user) {create(:user)}
   let(:camera) {create(:camera, is_public: true)}

   describe '#new' do
      context 'when provided with all valid and required data' do
         it 'creates a valid instance' do
            request = CameraShareRequest.new(user: user,
                                             camera: camera,
                                             email: "someone@nowhere.com",
                                             status: CameraShareRequest::PENDING,
                                             rights: "list,view")
            expect(request.valid?).to eq(true)
            expect(request.key).not_to be_nil
         end
      end

      context 'when a user is not specified' do
         it 'creates an invalid instance' do
            request = CameraShareRequest.new(camera: camera,
                                             email: "someone@nowhere.com",
                                             status: CameraShareRequest::PENDING,
                                             rights: "list,view")
            expect(request.valid?).to eq(false)
         end
      end

      context 'when a camera is not specified' do
         it 'creates an invalid instance' do
            request = CameraShareRequest.new(user: user,
                                             email: "someone@nowhere.com",
                                             status: CameraShareRequest::PENDING,
                                             rights: "list,view")
            expect(request.valid?).to eq(false)
         end
      end

      context 'when an email is not specified' do
         it 'creates an invalid instance' do
            request = CameraShareRequest.new(user: user,
                                             camera: camera,
                                             status: CameraShareRequest::PENDING,
                                             rights: "list,view")
            expect(request.valid?).to eq(false)
         end
      end

      context 'when the email address specified is too long' do
         it 'creates an invalid instance' do
            request = CameraShareRequest.new(user: user,
                                             camera: camera,
                                             status: CameraShareRequest::PENDING,
                                             email: ("A" * (CameraShareRequest::MAX_EMAIL_LEN + 1)),
                                             rights: "list,view")
            expect(request.valid?).to eq(false)
         end
      end

      context 'when a status is not specified' do
         it 'creates an invalid instance' do
            request = CameraShareRequest.new(user: user,
                                             camera: camera,
                                             email: "someone@nowhere.com",
                                             rights: "list,view")
            expect(request.valid?).to eq(false)
         end
      end

      context 'when an invalid status is specified' do
         it 'creates an invalid instance' do
            request = CameraShareRequest.new(user: user,
                                             camera: camera,
                                             email: "someone@nowhere.com",
                                             status: 1234,
                                             rights: "list,view")
            expect(request.valid?).to eq(false)
         end
      end

      context 'when a key that is too long is specified' do
         it 'creates an invalid instance' do
            request = CameraShareRequest.new(user: user,
                                             camera: camera,
                                             email: "someone@nowhere.com",
                                             status: CameraShareRequest::PENDING,
                                             key: ("A" * (CameraShareRequest::MAX_KEY_LEN + 1)),
                                             rights: "list,view")
            expect(request.valid?).to eq(false)
         end
      end

      context 'when rights are not specified' do
         it 'creates an invalid instance' do
            request = CameraShareRequest.new(user: user,
                                             camera: camera,
                                             email: "someone@nowhere.com",
                                             status: CameraShareRequest::PENDING)
            expect(request.valid?).to eq(false)
         end
      end

      context 'when the list of rights specified is too long' do
         it 'creates an invalid instance' do
            request = CameraShareRequest.new(user: user,
                                             camera: camera,
                                             email: "someone@nowhere.com",
                                             status: CameraShareRequest::PENDING,
                                             rights: ("A" * (CameraShareRequest::MAX_RIGHTS_LEN + 1)))
            expect(request.valid?).to eq(false)
         end
      end
   end
end