require "spec_helper"
require "georuby"

describe Geocoding do

  subject { Geocoding }

  WebMock.allow_net_connect!

  describe "::as_point" do

    let(:point) { GeoRuby::SimpleFeatures::Point.from_x_y(1, 2) }

    it "returns nil as itself" do
      expect(subject.as_point(nil)).to eq(nil)
    end

    it "returns a GeoRuby point as itself" do
      expect(subject.as_point(point)).to eq(point)
    end

    it "returns a hash with lat: and lng: symbols as a point" do
      expect(subject.as_point(lng: 1, lat: 2)).to eq(point)
    end

    it "returns a hex well known binary text as a point" do
      hewkb = "0101000000000000000000F03F0000000000000040"
      expect(subject.as_point(hewkb)).to eq(point)
    end

    it "returns a space separate lng lat pair as a point" do
      expect(subject.as_point("1.0 2.0")).to eq(point)
    end

    it "returns a comma separate lng lat pair as a point" do
      expect(subject.as_point("1.0, 2.0")).to eq(point)
    end

    it "returns correctly parses a pair with negative coordinates" do
      expect(subject.as_point("-1.0 -2.0")).to eq(
        GeoRuby::SimpleFeatures::Point.from_x_y(-1, -2))
    end

    it "geocodes an address to a point" do
      expect(subject.as_point("The North Pole")).to eq(
        GeoRuby::SimpleFeatures::Point.from_x_y(0, 90))
    end

    it "raises an error when the address cannot be geocoded" do
      expect{ subject.as_point("") }.to raise_error
    end

  end

end

