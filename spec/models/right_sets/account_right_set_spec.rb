require 'data_helper'

describe AccountRightSet do
	describe "accessors =>" do
		let(:user) { create(:user) }
		let(:client) { create(:client) }
		let(:rights) { AccountRightSet.new(user, client, AccessRight::CAMERAS) }

		describe "user" do
			it "returns the correct user" do
				expect(rights.user).to eq(user)
			end
		end

		describe "scope" do
			it "returns the correct scope setting" do
				expect(rights.scope).to eq(AccessRight::CAMERAS)
			end
		end
	end

	describe "rights checks" do
		let(:client) { create(:client) }
		let(:user) { create(:user) }
		let(:access_token) { create(:access_token, client: client) }
		let(:access_right) { create(:account_access_right, token: access_token, account: user) }
		let(:camera1) { create(:camera, owner: user, is_public: false) }
		let(:camera2) { create(:camera, owner: user, is_public: false) }

		describe "where the requester has an account level access right" do
			let(:access_right) { create(:account_access_right, token: access_token, account: user) }
      let(:rights) { AccountRightSet.new(user, client, AccessRight::CAMERAS) }

			before(:each) {access_right.save}

			it "returns true for the associated right" do
				expect(rights.allow?(AccessRight::VIEW)).to eq(true)
			end

			it "returns false for all other rights" do
				expect(rights.allow?(AccessRight::SNAPSHOT)).to eq(false)
				expect(rights.allow?(AccessRight::LIST)).to eq(false)
				expect(rights.allow?(AccessRight::EDIT)).to eq(false)
				expect(rights.allow?(AccessRight::DELETE)).to eq(false)
			end
		end

		describe "where the requester does not have an account level access right" do
   	  let(:rights) { AccountRightSet.new(user, client, AccessRight::CAMERAS) }

			it "returns false for all rights" do
				expect(rights.allow?(AccessRight::VIEW)).to eq(false)
				expect(rights.allow?(AccessRight::SNAPSHOT)).to eq(false)
				expect(rights.allow?(AccessRight::LIST)).to eq(false)
				expect(rights.allow?(AccessRight::EDIT)).to eq(false)
				expect(rights.allow?(AccessRight::DELETE)).to eq(false)
			end
		end

		describe "when accessed via individual resource right sets" do
			let(:rights) { AccessRightSet.for(camera1, client) }

			describe "and the requester has an account level access right" do
				let(:access_right) { create(:account_access_right, token: access_token, account: user) }

			  before(:each) {access_right.save}

				it "returns true for the associated right" do
					expect(rights.kind_of?(CameraRightSet)).to eq(true)
					expect(rights.allow?(AccessRight::VIEW)).to eq(true)
				end

				it "returns false for all other rights" do
					expect(rights.allow?(AccessRight::SNAPSHOT)).to eq(false)
					expect(rights.allow?(AccessRight::LIST)).to eq(false)
					expect(rights.allow?(AccessRight::EDIT)).to eq(false)
					expect(rights.allow?(AccessRight::DELETE)).to eq(false)
				end
			end

			describe "and the requester does not haves an account level access right" do
				it "returns false for all rights" do
					expect(rights.kind_of?(CameraRightSet)).to eq(true)
					expect(rights.allow?(AccessRight::VIEW)).to eq(false)
					expect(rights.allow?(AccessRight::SNAPSHOT)).to eq(false)
					expect(rights.allow?(AccessRight::LIST)).to eq(false)
					expect(rights.allow?(AccessRight::EDIT)).to eq(false)
					expect(rights.allow?(AccessRight::DELETE)).to eq(false)
				end
			end
		end
	end

   describe "#recorded_rights" do
      let(:account) { create(:user) }
      let!(:camera) { create(:camera) }

      describe "for clients" do
         let!(:client1) { create(:client) }
         let!(:client2) { create(:client) }
         let(:right_set) { AccountRightSet.new(account, client1, AccessRight::CAMERAS) }

         before(:each) do
            right_set.grant(AccessRight::VIEW, AccessRight::EDIT, AccessRight::DELETE)
         end

         it "returns an empty array for a client with no rights" do
            right_set = AccountRightSet.new(account, client2, AccessRight::CAMERAS)
            rights    = right_set.recorded_rights
            expect(rights).not_to be_nil
            expect(rights.class).to eq(Array)
            expect(rights.empty?).to eq(true)
         end

         it "returns a list of rights for a client that has access right records on the account" do
            rights    = right_set.recorded_rights
            expect(rights).not_to be_nil
            expect(rights.class).to eq(Array)
            expect(rights.empty?).to eq(false)
            expect(rights.size).to eq(3)
            expect(rights.include?(AccessRight::VIEW)).to eq(true)
            expect(rights.include?(AccessRight::EDIT)).to eq(true)
            expect(rights.include?(AccessRight::DELETE)).to eq(true)
         end
      end
   end
end