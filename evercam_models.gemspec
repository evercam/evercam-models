# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'evercam_models/version'

Gem::Specification.new do |spec|
  spec.name          = "evercam_models"
  spec.version       = EvercamModels::VERSION
  spec.authors       = ["Evercam.io"]
  spec.email         = ["howrya@evercam.io"]
  spec.summary       = %q{Evercam model classes library.}
  spec.description   = %q{This library contains all of the models used by the Evercam system.}
  spec.homepage      = "https://evercam.io"
  spec.license       = "Commercial"

  spec.files         = Dir.glob("{bin,lib}/**/*") + %w(LICENSE.md README.md CHANGELOG.md)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "database_cleaner", "~> 1.4"
  spec.add_development_dependency "factory_girl", "~> 4.5"
  spec.add_development_dependency "mocha", "~> 1.1"
  spec.add_development_dependency "rack-test", "~> 0.6.3"
  spec.add_development_dependency "rake", "~> 10.4"
  spec.add_development_dependency "rspec", "= 3.2.0"
  spec.add_development_dependency "webmock", "~> 1.21"
  spec.add_development_dependency "simplecov", "~> 0.10.0"

  spec.add_dependency "bcrypt", "~> 3.1"
  spec.add_dependency "dotenv", "~> 2.0"
  spec.add_dependency "evercam_misc", "~> 0.0.11"
  spec.add_dependency "georuby", "~> 2.3"
  spec.add_dependency "geocoder", "~> 1.2"
  spec.add_dependency "nokogiri", "~> 1.6"
  spec.add_dependency "pg", "~> 0.18.2"
  spec.add_dependency "sequel", "= 4.26.0"
  spec.add_dependency "timezone", "~> 0.3"
  spec.add_dependency "typhoeus", "~> 0.7.1"
  spec.add_dependency "activesupport", "~> 4.2"
  spec.add_dependency "hashie", "~> 3.4"
end
